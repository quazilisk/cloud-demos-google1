package demo.crud;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import demo.HibernateUtil;
import demo.model.Student;

public class InsertDemo {

  public static void main(String[] args) {

    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();
    session.beginTransaction();

    Student student = new Student();
    student.setFirstName("Bob");
    student.setAge(26);

    session.save(student);
    session.getTransaction().commit();

    session.close();

  }
}
