package demo.crud;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import demo.HibernateUtil;
import demo.model.Student;

public class UpdateDemo {

  public static void main(String[] args) {

    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();
    session.beginTransaction();

    List<Student> students = session.createQuery("from Student").list();
    if (!students.isEmpty()) {
      // Student student = (Student) session.get(Student.class, 1);
      Student student = students.get(0);
      student.setFirstName("Zet");
      student.setAge(45);

      session.update(student);
    }

    session.getTransaction().commit();

    session.close();
  }
}