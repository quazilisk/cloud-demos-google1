package demo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.google.appengine.api.utils.SystemProperty;

import demo.model.Student;


@SuppressWarnings("serial")
public class HibernateDemoServlet extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException,
      ServletException {
    String path = req.getRequestURI();
    if (path.startsWith("/favicon.ico")) {
      return;
    }
    tryHibernate(resp.getWriter());
  }

  private void tryHibernate2(PrintWriter out) {
    Map<String, String> properties = new HashMap();

    //<property name="hibernate.connection.url">jdbc:mysql://localhost:3306/try</property>
    if (SystemProperty.environment.value() == SystemProperty.Environment.Value.Production) {
      properties.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.GoogleDriver");
      properties.put("javax.persistence.jdbc.url", System.getProperty("cloudsql.url"));
    } else {
      properties.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
      properties.put("javax.persistence.jdbc.url", System.getProperty("cloudsql.url.dev"));
    }

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("Demo", properties);
  }

  private void tryHibernate(PrintWriter out) {
    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();
    session.beginTransaction();

    Random random = new Random();
    Student student = new Student();
    student.setFirstName("Bob" + random.nextInt(30));
    student.setAge(random.nextInt(100));

    session.save(student);
    session.getTransaction().commit();
    
    List<Student> students = session.createQuery("from Student").list();
    for (Student st : students) {
      out.println(st.getFirstName() + ", " + st.getAge());
    }

    session.close();
  }
}
